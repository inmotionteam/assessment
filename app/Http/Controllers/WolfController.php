<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Wolf;
use App\Location;

class WolfController extends Controller
{
    public function index()
    {
        return Wolf::with('location')->get();
    }

    public function show(Wolf $wolf)
    {
        return $wolf;
    }

    public function store(Request $request)
    {
        $wolf = Wolf::create($request->all());
        $location = Location::create(['wolf_id' => $wolf->id]);

        return response()->json($wolf, 201);
    }

    public function update(Request $request, Wolf $wolf)
    {
        $wolf->update($request->all());

        return response()->json($wolf, 200);
    }

    public function updateLocation(Request $request, $wolfId){
        $location = Location::where('wolf_id', $wolfId)->first();

        if (is_null($location)) {
            return false;
        }

        $request->request->add(['wolf_id' => $wolfId]);
        $location->update($request->all());

        return $location;
    }

    public function delete(Wolf $wolf)
    {
        $wolf->delete();

        return response()->json(null, 204);
    }
}
