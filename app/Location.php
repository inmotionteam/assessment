<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['wolf_id', 'lat', 'long', 'label'];
}
