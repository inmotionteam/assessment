<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wolf extends Model
{
    protected $fillable = ['name', 'gender', 'birthdate'];

    public function location()
    {
        return $this->hasOne('App\Location');
    }
}
