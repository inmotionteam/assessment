<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('wolves', 'WolfController@index');
Route::get('wolves/{wolf}', 'WolfController@show');
Route::post('wolves', 'WolfController@store');
Route::put('wolves/{wolf}', 'WolfController@update');
Route::put('wolves/{wolfId}/locations', 'WolfController@updateLocation');
Route::delete('wolves/{wolf}', 'WolfController@delete');

Route::get('locations', 'LocationController@index');
Route::get('locations/{location}', 'LocationController@show');
Route::post('locations', 'LocationController@store');
Route::put('locations/{location}', 'LocationController@update');
Route::delete('locations/{location}', 'LocationController@delete');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
